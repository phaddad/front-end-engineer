# Front End Engineer Profile #

[Detail List](https://docs.google.com/spreadsheets/d/1T0AM25JJapbi37Who9h-4T5xArL7MKW9CinNdjLY40Q/edit#gid=1437778151).

https://university.globant.com/search/trainings?keys=css


### Version Control System ###

* Reset 
* Revert
* Cherry-pick
* Merge
* rebase
	
### [CSS](https://university.globant.com/search/trainings?keys=css). ###





* [Selectors (specificity/pseudo)](https://developer.mozilla.org/en-US/docs/Web/CSS/Specificity)
* [Layouting (float, inline-block, position)](https://developer.mozilla.org/en-US/docs/Web/CSS/float)
* [Box-Model](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Box_Model/Introduction_to_the_CSS_box_model)
* Crossbrowsers
* Responsive
* Typography, Iconfonts
* Flexbox
* CSS Grid
* BEM / SMACSS / OOCSS
* CSS Transformations and Transitions
 

### CSS Preprocessors ###

* SASS
* Material Design

### HTML5 ###

* HTML Structure (Semantic HTML for 4+, doctypes, etc)
* SEO (Metatags, Open Graph, Server Side Rendering)
* Accessibility (Aria for 5+)
* Well-known APIs (localStorage/sessionStorage/connection/video/audio)
* High pixel density image support (retina, srcset, etc)

### JS Libraries ###

* Underscore

### JS Templating ###

* Handlebars

### JS ###

* Basic Language Concepts (types,async, this)
* DOM Manipulation
* Closure/Scoping/Hoisting
* OOP / Prototype (new/Object.create, constructors)
* Ajax (security, cors, json/jsonp, xml, etc)
* Functional programming (as opposed to OOP)
* Promises (q/deferred, chaining, pro/cons)
* App Structure (MV*)
* Events (capture/target/propagation, delegation, Touch Events Support)
* Patterns (Module/Observer/IIFE/mixin)
* Single page application (SPA, under the hood concepts as routing, etc)
* Debugging
* Require, CommonJS, ES6 Modules, etc
* Browserify, WebPack, Rollup.js
* HTTP2
* Websockets (e.g. socket.io)
* ES6/7 Concepts
* Typescript
* Canvas (Raw & Libraries like Paper.js, KinectJS, etc)
* WebComponents (concept/shadow DOM)
* Webworkers
	
### JS Frameworks ###

* Angular 2+

### Testing ###

* Concepts (Unit testing, mocks, stubs, asyncs, etc)
* Mocha
* Chai

### NodeJS ###

* APIs
* Cache
* Frameworks (Express/Hapi/Restify/etc)
* NoSQL/SQL
* Steps to Production (Logs, Cluster, Monitoring, Gracefully shutdown)
* Streams

### Project ###

* Agile methodologies (and tools)
* Linters
* npm
* Continuous Integration
