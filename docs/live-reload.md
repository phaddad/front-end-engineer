
## Live Reload install


    npm i -D nodemon livereload connect-livereload

Add to package.json scripts:

    "watch": "nodemon"

Example:

    "scripts": {
        "start": "node ./bin/www",
        "watch": "nodemon"
    },
