const express = require('express')
const path = require('path')
const PORT = process.env.PORT || 5001

const results = {results:[{ "id":1, "name":"first name!"}, { "id":2, "name":"second name"}]};

express()
  .use(express.static(path.join(__dirname, 'public')))
  .set('views', path.join(__dirname, 'views'))
  .set('view engine', 'ejs')
  .get('/', (req, res) => res.render('pages/index', results))
  
  .listen(PORT, () => console.log(`Listening on ${ PORT }`))
