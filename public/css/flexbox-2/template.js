function showContent(options, templateIndex, classContent) {
  var temp, label, iLabel, i;
  temp = document.getElementsByTagName("template")[templateIndex];
  label = temp.content.querySelector("label");
  input = temp.content.querySelector("input");
  options.forEach(item => {
    iLabel = document.importNode(label, true);
    iLabel.textContent = item.label;
    iLabel.setAttribute('for', classContent+item.label);

    ipt = document.importNode(input, true);
    ipt.value = item.label;
    ipt.id = classContent+item.label;
    ipt.checked = item.checked;

    document.body.querySelector('.' + classContent).appendChild(ipt);
    document.body.querySelector('.' + classContent).appendChild(iLabel);
  });
}