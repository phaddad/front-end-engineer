setTimeout(function(){ console.log("Hello")}, 2000);

function DelayedExecution(number){
    var deferred = Q.defer();
    setTimeout(()=> {
        number < 10 ? deferred.reject(new Error('Number is too small'))
                    : deferred.resolve('Number is the right size')
    },2500);
    return deferred.promise;
}

DelayedExecution(11).then(msg=> {
    console.log(msg);
}).catch(error =>{
    console.log(error);
})