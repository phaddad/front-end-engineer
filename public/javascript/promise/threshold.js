// Para experimentar con el manejo de errores, los valores de "threshold" 
// causan errores aleatoriamente
const THRESHOLD_A = 6; // puede usar cero 0 para garantizar el error

function tetheredGetNumber(resolve, reject) {
    try {
        setTimeout(
            function () {
                const randomInt = Date.now(); // 1679683037984
                const value = randomInt % 10; // [0...9]
                try {
                    if (value >= THRESHOLD_A) {
                        throw new Error(`(1) Demasiado grande: ${value}`);
                    }
                } catch (msg) {
                    reject(`(1.a) Error en el  callback: ${msg.message}`);
                }
                resolve(value);
                return;
            }, 500);
        // Para experimentar con un error en la configuración, descomente el siguiente 'throw'.
        // throw new Error("Mala configuración");
    } catch (err) {
        reject(`Error durante la configuración: ${err}`);
    }
    return;
}

function determineParity(value) {
    const isOdd = value % 2 ? true : false;
    const parityInfo = { theNumber: value, isOdd: isOdd };
    return parityInfo;
}

// when number is equal to THRESHOLD_A
function troubleWithGetNumber(reason) {
    console.error(`(1.e)Problemas para obtener el número: ${reason}`);
    throw -999; // debe "lanzar" algo, para mantener el estado de error en la cadena
}

function promiseGetWord(parityInfo) {
    // La función "tetheredGetWord()" obtiene "parityInfo" como variable de cierre.
    const tetheredGetWord = function (resolve, reject) {
        const theNumber = parityInfo.theNumber;
        const threshold_B = THRESHOLD_A - 1;
        if (theNumber === threshold_B) {
            reject(`(PW) Number ${theNumber} todavia demasiado grande que ${threshold_B} `);
        } else {
            parityInfo.wordEvenOdd = parityInfo.isOdd ? 'impar' : 'par';
            resolve(parityInfo);
        }
        return;
    }
    return new Promise(tetheredGetWord);
}

(new Promise(tetheredGetNumber))
    .then(determineParity, troubleWithGetNumber)
    .then(promiseGetWord) // si nunca da error se ejecuta esta linea
    .then((info) => {
        // when number <= THRESHOLD_A -2
        console.log("Got: ", info.theNumber, " , ", info.wordEvenOdd);
        return info;
    })
    .catch((reason) => {
        if (reason === -999) {
            // when number is equal to THRESHOLD_A
            console.error("Había manejado previamente el error", reason);
        }
        else {
            // when number is equal to THRESHOLD_A-1
            console.error(`Problema con promiseGetWord(): ${reason}`);
        }
    })
    .finally((info) => console.log("Todo listo"));
